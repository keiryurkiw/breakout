/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cglm/cglm.h>
#include <GL/glew.h>
#include <stddef.h>
#include <string.h>

#include "gameobj.h"
#include "sprite.h"

extern float delta_time;

Gameobj
gameobj_create(char *tex_path)
{
	Gameobj obj = { 0 };
	obj.sprite = sprite_create(tex_path);
	obj.vbo = sprite_gen_info_vbo(&obj.sprite, 1, GL_DYNAMIC_DRAW);

	return obj;
}

void
gameobj_set_info(Gameobj *obj)
{
	sprite_set_info(0, &obj->sprite, obj->vbo,
			obj->pos, obj->size, obj->colour);
}

void
gameobj_draw(Gameobj *obj, unsigned int program)
{
	glUseProgram(program);
	glActiveTexture(GL_TEXTURE0);

	glBindVertexArray(obj->sprite.vao);
	glBindTexture(GL_TEXTURE_2D, obj->sprite.texture);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void
gameobj_free(Gameobj *obj)
{
	sprite_free(&obj->sprite);
	memset(obj, 0, sizeof(*obj));
}

Ball
ball_create(char *tex_path, float radius)
{
	Ball ball;
	ball.obj = gameobj_create(tex_path);
	ball.stuck = true;

	float r2 = radius * 2.0f;

	glm_vec3_copy((vec3){ 1.0f, 1.0f, 1.0f }, ball.obj.colour);
	glm_vec2_copy((vec2){ r2, r2, }, ball.obj.size);
	gameobj_set_info(&ball.obj);

	return ball;
}

void
ball_move(Ball *ball, int win_width)
{
	if (ball->stuck)
		return;

	vec2 v;
	glm_vec2_scale(ball->obj.velocity, delta_time, v);
	glm_vec2_add(ball->obj.pos, v, ball->obj.pos);

	if (ball->obj.pos[0] <= 0.0f) {
		ball->obj.velocity[0] = -ball->obj.velocity[0];
		ball->obj.pos[0] = 0.0f;
	} else if (ball->obj.pos[0] + ball->obj.size[0] >= (float)win_width) {
		ball->obj.velocity[0] = -ball->obj.velocity[0];
		ball->obj.pos[0] = win_width - ball->obj.size[0];
	}

	if (ball->obj.pos[1] <= 0.0f) {
		ball->obj.velocity[1] = -ball->obj.velocity[1];
		ball->obj.pos[1] = 0.0f;
	}

	gameobj_set_info(&ball->obj);
}

void
ball_free(Ball *ball)
{
	gameobj_free(&ball->obj);
	memset(ball, 0, sizeof(*ball));
}
