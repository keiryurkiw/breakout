# Breakout

Breakout is a tutorial engine I wrote in C with the OpenGL graphics API. It was
originally written by Joey de Vries in C++ in his series
[LearnOpenGL](https://learnopengl.com/In-Practice/2D-Game/Breakout).

The game currently does not officially support compilation or running on
Windows. Although theoretically possible, it has not been tested and support is
not planned in the future.

## Dependencies
1. C11-compliant compiler
2. GLFW 3
3. GLEW
4. cglm
5. libao
6. FreeType 2

## Building

Use any POSIX-compliant Make to build Breakout

```bash
make clean all
```

## Usage

Run the game with the following command

```bash
./breakout
```

## License

[GPLv3](https://www.gnu.org/licenses/#GPL)
