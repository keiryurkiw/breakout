# Copyright (C) 2022, 2023  Keir Yurkiw

.POSIX:
.SUFFIXES:

# Development flags
WARNINGS = -Wall -Wextra -Wmissing-prototypes -Wdouble-promotion \
           -Wno-char-subscripts -pedantic-errors
#CC = gcc
#CFLAGS = -O0 -g -pipe
#CFLAGS += $(WARNINGS) -std=c17
#CFLAGS += -fanalyzer
#CPPFLAGS =


# Default options
PREFIX = /usr/local
PKG_CONFIG = pkg-config
#CC = gcc
#CFLAGS = -O2 -pipe -w
#CPPFLAGS = -DNDEBUG


PKGS = glfw3 glew cglm ao freetype2
GLCPPFLAGS = `$(PKG_CONFIG) --cflags $(PKGS)` \
             -D_POSIX_C_SOURCE=200809L $(CPPFLAGS)
LDLIBS = `$(PKG_CONFIG) --libs $(PKGS)` -lm

include src.mk

all: breakout

$(OBJ): Makefile
$(OBJ): $(HEADERS)

.SUFFIXES: .c .o
.c.o:
	$(CC) $(GLCPPFLAGS) $(CFLAGS) -c $< -o $@

breakout: $(OBJ)
	$(CC) -o $@ $(OBJ) $(LDFLAGS) $(LDLIBS)

clean:
	rm -f breakout $(OBJ)

.PHONY: all clean install uninstall
