/*
 * Copyright (C) 2022, 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <cglm/cglm.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>

#include "audio.h"
#include "gameobj.h"
#include "level.h"
#include "opengl.h"
#include "sprite.h"
#include "text.h"
#include "utils.h"
#include "window.h"

#define FONT "/usr/share/fonts/liberation/LiberationSans-Regular.ttf"
#define PLAYER_VELOCITY 500.0f
#define PADDLE_PADDING 2.0f
#define BALL_RADIUS 12.5f
enum { DEFAULT_PLAYER_LIVES = 3 };

typedef enum {
	GAME_MENU,
	GAME_ACTIVE,
	GAME_WIN,
	GAME_LOSE,
} GameState;

float delta_time = -1.0f;
const vec2 initial_ball_velocity = { 100.0f, -350.0f };

static int level_number = 1;
static GameState game_state = GAME_MENU;
static int player_lives = DEFAULT_PLAYER_LIVES;

static void
reset_player(Gameobj *player, int win_width, int win_height)
{
	glm_vec3_copy((vec3){ 1.0f, 1.0f, 1.0f }, player->colour);
	glm_vec2_copy((vec2){ 100.0f, 20.0f }, player->size);
	glm_vec2_copy((vec2){ win_width/2.0f - player->size[0]/2.0f,
			win_height - (player->size[1]+PADDLE_PADDING) }, player->pos);
	gameobj_set_info(player);
}

static void
reset_ball(Ball *ball, Gameobj *player)
{
	ball->obj.velocity[0] = initial_ball_velocity[0];
	ball->obj.velocity[1] = initial_ball_velocity[1];
	glm_vec2_add((vec2){ player->size[0]*0.5f - BALL_RADIUS,
			-BALL_RADIUS*2.0f }, player->pos, ball->obj.pos);
	gameobj_set_info(&ball->obj);

	ball->stuck = true;
}

static void
switch_levels(GLFWwindow *window, Level *cur_level, unsigned int level_num)
{
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	Sprite block = cur_level->block;
	Sprite solid = cur_level->solid_block;

	char path[256];
	sprintf(path, "assets/levels/level%d.txt", level_num);

	level_free(cur_level);
	*cur_level = level_load(path, block, solid, width, height);
}

static void
reset_to_menu(GLFWwindow *window, Level *level, Gameobj *player, Ball *ball)
{
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	reset_player(player, width, height);
	reset_ball(ball, player);
	level_reload(level, width, height);
	player_lives = DEFAULT_PLAYER_LIVES;
	game_state = GAME_MENU;
}

static inline void
process_input(GLFWwindow *window, Level *level, Gameobj *player, Ball *ball)
{
	switch (game_state) {
		case GAME_MENU:
			if (glfwGetKey(window, GLFW_KEY_BACKSPACE) == GLFW_PRESS)
				glfwSetWindowShouldClose(window, true);

			if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS) {
				ball->stuck = false;
				game_state = GAME_ACTIVE;
			}

			static bool right_pressed = false, left_pressed = false;;
			if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
				if (!right_pressed) {
					level_number = clampi(level_number+1, 1, 4);
					switch_levels(window, level, level_number);
				}

				right_pressed = true;
			} else {
				right_pressed = false;
			}

			if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
				if (!left_pressed) {
					level_number = clampi(level_number-1, 1, 4);
					switch_levels(window, level, level_number);
				}

				left_pressed = true;
			} else {
				left_pressed = false;
			}

			break;
		case GAME_ACTIVE:
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
				reset_to_menu(window, level, player, ball);

			const float velocity = PLAYER_VELOCITY * delta_time;

			int width, height;
			glfwGetFramebufferSize(window, &width, &height);

			float *pos = &player->pos[0], size = player->size[0];
			const float right_extent = width - size - PADDLE_PADDING;
			const float left_extent = PADDLE_PADDING;
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
				*pos = clampf(*pos - velocity, left_extent, right_extent);
			}
			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
				*pos = clampf(*pos + velocity, left_extent, right_extent);

			/* Out-of-bounds */
			if (ball->obj.pos[1] >= height) {
				reset_player(player, width, height);
				reset_ball(ball, player);

				if (--player_lives == 0)
					game_state = GAME_LOSE;
				else
					ball->stuck = false;
			}

			gameobj_set_info(player);

			if (level_check_win(level))
				game_state = GAME_WIN;

			break;
		case GAME_WIN:
		case GAME_LOSE:
			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
				reset_to_menu(window, level, player, ball);

			break;
	}
}

static int
play_music(void *window)
{
	WindowInfo *winfo = glfwGetWindowUserPointer(window);
	Audio *music = winfo->music_ptr;

	size_t offset = 0, bytes = music->sample_rate;
	int extra = -1;
	while (!glfwWindowShouldClose(window)) {
		if (offset+bytes >= music->size) {
			extra = offset+bytes - music->size;
			bytes -= extra;
		}

		audio_play_offset(music, offset, bytes);

		if (extra >= 0) {
			extra = -1;
			offset = 0;
			bytes = music->sample_rate;
		} else {
			offset += music->sample_rate;
		}
	}

	return 0;
}

static inline void
render_text(GLFWwindow *window, Font *font, unsigned int font_program)
{
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	char string[256];
	float txt_len;
	vec2 pos;

	switch (game_state) {
		case GAME_MENU:
			strcpy(string, "Press ENTER to begin");
			txt_len = text_get_len(string, font, 1.0f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 }, pos);
			text_render(string, font, pos, 1.0f, 0xebdbb2, font_program);

			strcpy(string, "Press LEFT or RIGHT arrow to select level");
			txt_len = text_get_len(string, font, 0.75f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 + 25.0f }, pos);
			text_render(string, font, pos, 0.75f, 0xebdbb2, font_program);

			strcpy(string, "Press BACKSPACE to exit");
			txt_len = text_get_len(string, font, 0.75f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 + 100.0f }, pos);
			text_render(string, font, pos, 0.75f, 0xebdbb2, font_program);

			sprintf(string, "Level %d", level_number);
			glm_vec2_copy((vec2){ 5.0f, width - 225.0f }, pos);
			text_render(string, font, pos, 1.0f, 0xebdbb2, font_program);

			break;
		case GAME_ACTIVE:
			sprintf(string, "Lives: %d", player_lives);
			glm_vec2_copy((vec2){ 5.0f, 5.0f }, pos);
			text_render(string, font, pos, 1.0f, 0xebdbb2, font_program);

			break;
		case GAME_WIN:
			strcpy(string, "You win!");
			txt_len = text_get_len(string, font, 1.0f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 - 50.0f }, pos);
			text_render(string, font, pos, 1.0f, 0xebdbb2, font_program);

			strcpy(string, "Press ESC to return to the menu");
			txt_len = text_get_len(string, font, 0.75f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 + 25.0f }, pos);
			text_render(string, font, pos, 0.75f, 0xebdbb2, font_program);

			break;
		case GAME_LOSE:
			strcpy(string, "You lost!");
			txt_len = text_get_len(string, font, 1.0f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 - 50.0f }, pos);
			text_render(string, font, pos, 1.0f, 0xebdbb2, font_program);

			strcpy(string, "Press ESC to return to the menu");
			txt_len = text_get_len(string, font, 0.75f);
			glm_vec2_copy((vec2){ width/2 - txt_len, height/2 + 25.0f }, pos);
			text_render(string, font, pos, 0.75f, 0xebdbb2, font_program);

			break;
	}
}

int
main(void)
{
	if (!glfw_init())
		return EXIT_FAILURE;

	GLFWwindow *window = glfw_create_window();
	if (!window) {
		glfw_cleanup(NULL);
		return EXIT_FAILURE;
	}

	if (!ogl_init(window)) {
		glfw_cleanup(window);
		return EXIT_FAILURE;
	}

	unsigned int font_program = ogl_create_simple_shader((char *[]){
			"shaders/font.vert.glsl",
			"shaders/font.frag.glsl" });
	if (!font_program) {
		glfw_cleanup(window);
		return EXIT_FAILURE;
	}

	unsigned int sprite_program = ogl_create_simple_shader((char *[]){
			"shaders/sprite.vert.glsl",
			"shaders/sprite.frag.glsl" });
	if (!sprite_program) {
		glfw_cleanup(window);
		return EXIT_FAILURE;
	}

	enum { BIND_POINT = 0 };
	unsigned int projection_ubo = ogl_allocate_ubo(sizeof(mat4), GL_STATIC_DRAW,
			BIND_POINT, "matrices", 2, sprite_program, font_program);

	Background background = ogl_load_background("assets/background.jpg",
			sprite_program);

	audio_init();
	Audio music = audio_load("assets/audio/breakout.mp3");

	WindowInfo winfo = {
		.projection_ubo = projection_ubo,
		.background_ptr = &background,
		.music_ptr = &music,
	};
	glfwSetWindowUserPointer(window, &winfo);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	ogl_set_background_size(&background, width, height);
	ogl_set_projection(window, width, height);

	Sprite block = sprite_create("assets/sprites/block.png");
	Sprite solid_block = sprite_create("assets/sprites/block_solid.png");

	Gameobj player = gameobj_create("assets/sprites/paddle.png");
	reset_player(&player, width, height);

	Ball ball = ball_create("assets/sprites/awesomeface.png", BALL_RADIUS);
	reset_ball(&ball, &player);

	void *ft = text_init();
	Font font = text_load_font(ft, FONT, 24);

	Level level = level_load("assets/levels/level1.txt",
			block, solid_block, width, height);

	thrd_t audio_thread, win_title_thread;
	thrd_create(&audio_thread, play_music, window);
	thrd_create(&win_title_thread, glfw_set_window_fps_title, window);

	glfwShowWindow(window);
	while (!glfwWindowShouldClose(window)) {
		const float start_time = glfwGetTime();

		glfwPollEvents();
		process_input(window, &level, &player, &ball);

		ball_move(&ball, width);
		level_check_collisions(&level, &player, &ball);

		/* No need to clear the screen because we're drawing over it */
		ogl_draw_background(&background);

		level_draw(&level, sprite_program);

		gameobj_draw(&player, sprite_program);
		gameobj_draw(&ball.obj, sprite_program);

		render_text(window, &font, font_program);

		glfwSwapBuffers(window);

		delta_time = (float)glfwGetTime() - start_time;
	}
	glfwHideWindow(window);

	text_delete_font(&font);
	text_deinit(&ft);

	ball_free(&ball);
	level_free(&level);
	sprite_free(&block);
	sprite_free(&solid_block);

	ogl_delete_background(&background);

	glDeleteProgram(sprite_program);

	thrd_join(audio_thread, NULL);
	audio_free(&music);
	audio_deinit();

	thrd_join(win_title_thread, NULL);
	glfw_cleanup(window);
}
