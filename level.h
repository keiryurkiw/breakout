/* Copyright (C) 2023  Keir Yurkiw */

#ifndef LEVEL_H
#define LEVEL_H

#include <cglm/types.h>
#include <stdbool.h>

#include "gameobj.h"
#include "sprite.h"

typedef enum {
	DIR_NONE = -1,
	DIR_NORTH,
	DIR_EAST,
	DIR_SOUTH,
	DIR_WEST,
} Dir;

typedef struct {
	vec2 dir_vec;
	int dir;
	bool collided;
} Collision;

typedef struct {
	vec2 pos, size;
	bool is_destroyed;
} LevelBrick;

typedef struct {
	Sprite block, solid_block;
	LevelBrick *block_bricks, *solid_block_bricks;
	unsigned int block_vbo, solid_block_vbo;
	unsigned int block_count, solid_block_count;

	char *path;

	unsigned int width, height;
	float unit_width, unit_height;

	bool is_valid;
} Level;

Level level_load(char *path, Sprite block, Sprite solid_block,
		int win_width, int win_height);
void level_check_collisions(Level *level, Gameobj *player, Ball *ball);
bool level_check_win(Level *level);
void level_draw(Level *level, unsigned int program);
void level_free(Level *level);
void level_reload(Level *level, int win_width, int win_height);

#endif /* LEVEL_H */
