/*
 * Copyright (C) 2022, 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <cglm/cglm.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h> /* GLFW must be included after GLEW */
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "opengl.h"
#include "sprite.h"
#include "utils.h"
#include "window.h"

#define PROJ_NEAR 0.01f
#define PROJ_FAR 100.0f

#ifndef NDEBUG
static char *
ogl_get_type_str(GLenum type)
{
	switch (type) {
		case GL_DEBUG_TYPE_ERROR: return "Error";
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "Deprecated behaviour";
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "Undefined behaviour";
		case GL_DEBUG_TYPE_PORTABILITY: return "Portablility";
		case GL_DEBUG_TYPE_PERFORMANCE: return "Performance";
		case GL_DEBUG_TYPE_MARKER: return "Marker";
		case GL_DEBUG_TYPE_PUSH_GROUP: return "Push group";
		case GL_DEBUG_TYPE_POP_GROUP: return "Pop group";
		case GL_DEBUG_TYPE_OTHER: return "Other";
		default: return "Unknown";
	}
}

static char *
ogl_get_severity_str(GLenum severity)
{
	switch (severity) {
		case GL_DEBUG_SEVERITY_HIGH: return "High";
		case GL_DEBUG_SEVERITY_MEDIUM: return "Medium";
		case GL_DEBUG_SEVERITY_LOW: return "Low";
		case GL_DEBUG_SEVERITY_NOTIFICATION: return "Notification";
		default: return "Unknown";
	}
}

static char *
ogl_get_source_str(GLenum source)
{
	switch (source) {
		case GL_DEBUG_SOURCE_API: return "API";
		case GL_DEBUG_SOURCE_SHADER_COMPILER: return "Shader compiler";
		case GL_DEBUG_SOURCE_THIRD_PARTY: return "Third party";
		case GL_DEBUG_SOURCE_APPLICATION: return "Application";
		case GL_DEBUG_SOURCE_OTHER: return "Other";
		default: return "Unknown";
	}
}

static void APIENTRY
ogl_error_callback(GLenum source, GLenum type, unsigned int id, GLenum severity,
		GLsizei length, const char *message, const void *user_param)
{
	(void)user_param;
	(void)length;

	VPERROR(
			"OpenGL: %s\n"
			"Message ID: %u\n"
			"Type: %s\n"
			"Severity: %s\n"
			"Source: %s",
			message, id,
			ogl_get_type_str(type),
			ogl_get_severity_str(severity),
			ogl_get_source_str(source));
}
#endif /* NDEBUG */

bool
ogl_init(void *win)
{
	glfwMakeContextCurrent(win);

	int ret = glewInit();
	if (ret != GLEW_OK) {
		VPERROR("Failed to initialize OpenGL: %s", glewGetString(ret));
		return false;
	}

	if (!GLEW_VERSION_3_3) {
		PERROR("OpenGL version 3.3 is required but could not be aquired");
		return false;
	}

#ifndef NDEBUG
	int flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(ogl_error_callback, NULL);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
				0, NULL, GL_TRUE);
	} else {
		PERROR("OpenGL debug mode is not available");
	}
#endif /* NDEBUG */

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glCullFace(GL_BACK);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_MULTISAMPLE);

	return true;
}

void
ogl_set_vertex_attrib_pointer(size_t pos, size_t count,
		size_t stride, size_t offset)
{
	glEnableVertexAttribArray(pos);
	glVertexAttribPointer(pos, count, GL_FLOAT, GL_FALSE,
			stride, (void *)offset);
}

unsigned int
ogl_compile_shader(int type, char *path)
{
	File src = map_file(path);
	if (!src.data)
		return 0;

	unsigned int shader = glCreateShader(type);
	/* cast away const */
	glShaderSource(shader, 1, (const char **)&src.data, NULL);
	glCompileShader(shader);

	unmap_file(&src);

	int status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status) {
		int log_len;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);

		char *log = ALLOC(log_len);
		glGetShaderInfoLog(shader, log_len, NULL, log);
		if (log[log_len-2] == '\n')
			log[log_len-2] = '\0';

		VPERROR("Failed to compile shader '%s': %s", path, log);

		FREE(log);

		return 0;
	}

	return shader;
}

unsigned int
ogl_link_shaders(unsigned int count, ...)
{
	unsigned int *shaders = ALLOC(count * sizeof(*shaders));

	va_list shader_list;
	va_start(shader_list, count);
	for (size_t i = 0; i < count; i++)
		shaders[i] = va_arg(shader_list, unsigned int);
	va_end(shader_list);

	unsigned int program = glCreateProgram();
	for (size_t i = 0; i < count; i++)
		glAttachShader(program, shaders[i]);
	glLinkProgram(program);

	for (size_t i = 0; i < count; i++)
		glDetachShader(program, shaders[i]);
	FREE(shaders);

	int status;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status) {
		int log_len;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len);

		char *log = ALLOC(log_len);
		glGetProgramInfoLog(program, log_len, NULL, log);
		if (log[log_len-2] == '\n')
			log[log_len-2] = '\0';

		VPERROR("Failed to link shaders: %s", log);

		FREE(log);

		return 0;
	}

	return program;
}

void
ogl_delete_shaders(unsigned int count, ...)
{
	va_list shaders;
	va_start(shaders, count);
	for (size_t i = 0; i < count; i++)
		glDeleteShader(va_arg(shaders, unsigned int));
	va_end(shaders);
}

unsigned int
ogl_create_simple_shader(char **paths)
{
	unsigned int vert = ogl_compile_shader(GL_VERTEX_SHADER, paths[0]);
	if (!vert)
		return 0;

	unsigned int frag = ogl_compile_shader(GL_FRAGMENT_SHADER, paths[1]);
	if (!frag) {
		ogl_delete_shaders(1, vert);
		return 0;
	}

	unsigned int program = ogl_link_shaders(2, vert, frag);
	ogl_delete_shaders(2, vert, frag);
	if (!program) {
		PERROR("Failed to create shader program");
		return 0;
	}

	return program;
}

int
ogl_get_unif_loc(unsigned int program, char *name)
{
	int loc = glGetUniformLocation(program, name);
	if (loc < 0)
		VPERROR("Failed to find uniform '%s'", name);

	return loc;
}

void
ogl_set_vec4(int loc, vec4 vector)
{
	glUniform4fv(loc, 1, vector);
}

void
ogl_set_mat4(int loc, mat4 matrix)
{
	glUniformMatrix4fv(loc, 1, GL_FALSE, matrix[0]);
}

void
ogl_set_ubo_mat4(unsigned int ubo, size_t offset, mat4 mat)
{
	glBindBuffer(GL_UNIFORM_BUFFER, ubo);
	glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(mat4), mat[0]);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

unsigned int
ogl_allocate_ubo(size_t size, int usage, unsigned int bind_point,
		char *name, unsigned int nprograms, ...)
{
	unsigned int ubo;
	glGenBuffers(1, &ubo);

	glBindBuffer(GL_UNIFORM_BUFFER, ubo);
	glBufferData(GL_UNIFORM_BUFFER, size, NULL, usage);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glBindBufferBase(GL_UNIFORM_BUFFER, bind_point, ubo);

	va_list programs;
	va_start(programs, nprograms);
	for (size_t i = 0; i < nprograms; i++) {
		unsigned int program = va_arg(programs, unsigned int);
		unsigned int index = glGetUniformBlockIndex(program, name);
		glUniformBlockBinding(program, index, bind_point);
	}
	va_end(programs);

	return ubo;
}

bool
ogl_set_projection(void *win, int width, int height)
{
	WindowInfo *winfo = glfwGetWindowUserPointer(win);
	if (!winfo) {
		PERROR("Failed to set matrices: "
				"GLFW window user pointer has not been set");
		return false;
	}

	mat4 projection;
	glm_ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f, projection);

	glViewport(0, 0, width, height);
	ogl_set_ubo_mat4(winfo->projection_ubo, 0, projection);

	return true;
}

unsigned int
ogl_gen_texture(char *path)
{
	Image texture = load_image(path);
	if (!texture.data)
		return 0;

	int format;
	switch (texture.num_channels) {
		case 1: format = GL_RED; break;
		case 2: format = GL_RG; break;
		case 3: format = GL_RGB; break;
		case 4: format = GL_RGBA; break;
		default: format = 0;
	}

	unsigned int id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, format, texture.width, texture.height,
			0, format, GL_UNSIGNED_BYTE, texture.data);

	free_image(&texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return id;
}

Background
ogl_load_background(char *path, unsigned int program)
{
	Background bg;
	bg.sprite = sprite_create(path);
	bg.vbo = sprite_gen_info_vbo(&bg.sprite, 1, GL_STREAM_DRAW);
	bg.program = program;

	return bg;
}

void
ogl_set_background_size(Background *bg, float width, float height)
{
	vec2 pos = { 0.0f, 0.0f };
	vec2 size = { width, height };
	vec3 colour = { 1.0f, 1.0f, 1.0f };
	sprite_set_info(0, &bg->sprite, bg->vbo, pos, size, colour);
}

void
ogl_draw_background(Background *bg)
{
	glUseProgram(bg->program);
	glBindVertexArray(bg->sprite.vao);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, bg->sprite.texture);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void
ogl_delete_background(Background *bg)
{
	glDeleteBuffers(1, &bg->vbo);
	sprite_free(&bg->sprite);
	memset(bg, 0, sizeof(*bg));
}
