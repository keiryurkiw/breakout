/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <cglm/cglm.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include <GL/glew.h>
#include <stdbool.h>
#include <stddef.h>

#include "opengl.h"
#include "text.h"
#include "utils.h"

void *
text_init(void)
{
	FT_Library *ft = ALLOC(sizeof(*ft));
	if (FT_Init_FreeType(ft) != 0) {
		PERROR("Failed to initialize FreeType2");
		FREE(ft);
		return NULL;
	}

	return ft;
}

Font
text_load_font(void *ft, char *path, unsigned int size)
{
	FT_Library *lib = ft;

	FT_Face face;
	if (FT_New_Face(*lib, path, 0, &face) != 0) {
		VPERROR("Failed to load font '%s'", path);
		return (Font){ 0 };
	} else {
		FT_Set_Pixel_Sizes(face, 0, size);
	}

	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	/* 2 positions + 2 tex coords * 4 vertices */
	const size_t vert_size = (2 + 2) * 4 * sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, vert_size, NULL, GL_DYNAMIC_DRAW);
	ogl_set_vertex_attrib_pointer(0, 4, 4 * sizeof(float), 0);

	Font font = {
		.vao = vao,
		.vbo = vbo,
	};

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (int c = 0; c < 128; c++) {
		if (FT_Load_Char(face, c, FT_LOAD_RENDER) != 0) {
			VPERROR("Failed to load glyph '%c'", c);
			continue;
		}

		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,
				face->glyph->bitmap.width, face->glyph->bitmap.rows,
				0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		font.chars[c] = (Char){
			.texture = texture,
			.size[0] = face->glyph->bitmap.width,
			.size[1] = face->glyph->bitmap.rows,
			.bearing[0] = face->glyph->bitmap_left,
			.bearing[1] = face->glyph->bitmap_top,
			.advance = face->glyph->advance.x,
		};
	}

	FT_Done_Face(face);

	font.is_valid = true;
	return font;
}

void
text_render(char *str, Font *font, vec2 pos, float scale,
		unsigned long col, unsigned int prog)
{
	glUseProgram(prog);

	vec3 colour;
	vec4 v4_col;
	hex_to_rgb(col, colour);
	glm_vec4(colour, 1.0f, v4_col);

	ogl_set_vec4(ogl_get_unif_loc(prog, "text_colour"), v4_col);

	glBindVertexArray(font->vao);
	glBindBuffer(GL_ARRAY_BUFFER, font->vbo);
	glActiveTexture(GL_TEXTURE0);

	float xpos = pos[0], ypos = pos[1];
	for (size_t i = 0; str[i] != '\0'; i++) {
		Char ch = font->chars[str[i]];

		float x = xpos + ch.bearing[0] * scale;
		float y = ypos + (font->chars['H'].bearing[1] - ch.bearing[1]) * scale;

		float w = ch.size[0] * scale;
		float h = ch.size[1] * scale;

		float vertices[] = {
			/* pos     tex  */
			x,   y+h,  0.0f, 1.0f,
			x,   y,    0.0f, 0.0f,
			x+w, y+h,  1.0f, 1.0f,
			x+w, y,    1.0f, 0.0f,
		};

		glBindTexture(GL_TEXTURE_2D, ch.texture);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		xpos += (ch.advance >> 6) * scale;
	}
}

float
text_get_len(char *str, Font *font, float scale)
{
	float length = 0.0f;
	for (size_t i = 0; str[i] != '\0'; i++)
		length += font->chars[str[i]].advance >> 6;

	return length*0.5f * scale;
}

void
text_delete_font(Font *font)
{
	for (size_t i = 0; i < 128; i++)
		glDeleteTextures(1, &font->chars[i].texture);

	memset(font, 0, sizeof(*font));
}

void
text_deinit(void **ft)
{
	FT_Library *lib = *ft;
	FT_Done_FreeType(*lib);
	FREE(*ft);
}
