/*
 * Copyright (C) 2022, 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <stdbool.h>
#include <threads.h>

#include "opengl.h"
#include "utils.h"
#include "window.h"

#define WIN_TITLE "Breakout"
enum { DEFAULT_WIN_WIDTH = 800, DEFAULT_WIN_HEIGHT = 600 };

extern float delta_time;

static void
glfw_error_callback(int code, const char *desc)
{
	char *str;
	switch (code) {
		case GLFW_NOT_INITIALIZED: str = "GLFW_NOT_INITIALIZED"; break;
		case GLFW_NO_CURRENT_CONTEXT: str = "GLFW_NO_CURRENT_CONTEXT"; break;
		case GLFW_INVALID_ENUM: str = "GLFW_INVALID_ENUM"; break;
		case GLFW_INVALID_VALUE: str = "GLFW_INVALID_VALUE"; break;
		case GLFW_OUT_OF_MEMORY: str = "GLFW_OUT_OF_MEMORY"; break;
		case GLFW_API_UNAVAILABLE: str = "GLFW_API_UNAVAILABLE"; break;
		case GLFW_VERSION_UNAVAILABLE: str = "GLFW_VERSION_UNAVAILABLE"; break;
		case GLFW_PLATFORM_ERROR: str = "GLFW_PLATFORM_ERROR"; break;
		case GLFW_FORMAT_UNAVAILABLE: str = "GLFW_FORMAT_UNAVAILABLE"; break;
		case GLFW_NO_WINDOW_CONTEXT: str = "GLFW_NO_WINDOW_CONTEXT"; break;
		default: str = "unknown error code";
	}

	VPERROR("glfw: 0x%08x (%s): %s", code, str, desc);
}

bool
glfw_init(void)
{
	if (!glfwInit()) {
		PERROR("GLFW initialization failed");
		return false;
	}

	glfwSetErrorCallback(glfw_error_callback);

	return true;
}

static bool
set_window_icon(GLFWwindow *window, char **paths)
{
	enum { ICON_COUNT = 2 };

	Image icons[ICON_COUNT];
	GLFWimage glfw_icons[ICON_COUNT];

	for (size_t i = 0; i < ICON_COUNT; i++) {
 		icons[i] = load_image(paths[i]);
		if (!icons[i].data) {
			PERROR("Failed to set window icon");

			for (size_t j = 0; j < i; j++)
				free_image(&icons[j]);
			return false;
		}

		glfw_icons[i].pixels = icons[i].data;
		glfw_icons[i].width = icons[i].width;
		glfw_icons[i].height = icons[i].height;
	}

	glfwSetWindowIcon(window, ICON_COUNT, glfw_icons);

	for (size_t i = 0; i < ICON_COUNT; i++)
		free_image(&icons[i]);

	return true;
}

static void
framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	WindowInfo *winfo = glfwGetWindowUserPointer(window);
	if (winfo) {
		ogl_set_background_size(winfo->background_ptr, width, height);
	}

	ogl_set_projection(window, width, height);
}

void *
glfw_create_window(void)
{
	glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
	glfwWindowHint(GLFW_FOCUSED, GLFW_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifndef NDEBUG
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif /* NDEBUG */

	void *window = glfwCreateWindow(DEFAULT_WIN_WIDTH, DEFAULT_WIN_HEIGHT,
			WIN_TITLE, NULL, NULL);
	if (!window) {
		PERROR("Failed to create window");
		glfwTerminate();
		return NULL;
	}

	char *icons[] = {
		"assets/icons/controller.png",
		"assets/icons/controller-small.png",
	};
	set_window_icon(window, icons);

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	return window;
}

int
glfw_set_window_fps_title(void *window)
{
	char title[256];

	while (!glfwWindowShouldClose(window)) {
		sprintf(title, WIN_TITLE " | FPS: %d", (int)roundf(1.0f / delta_time));
		glfwSetWindowTitle(window, title);

		thrd_sleep(&(struct timespec){ .tv_sec = 1 }, NULL);
	}

	return 0;
}

void
glfw_cleanup(void *window)
{
	if (window)
		glfwDestroyWindow(window);
	glfwTerminate();
}
