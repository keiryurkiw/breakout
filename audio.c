/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <ao/ao.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "audio.h"
#define DR_MP3_IMPLEMENTATION
#define DR_MP3_NO_STDIO
#include "third_party/dr_mp3.h"
#include "utils.h"

void
audio_init(void)
{
	ao_initialize();
}

void
audio_deinit(void)
{
	ao_shutdown();
}

static void *
dr_malloc(size_t size, void *user_data)
{
	(void)user_data;
	return ALLOC(size);
}

static void *
dr_realloc(void *ptr, size_t size, void *user_data)
{
	(void)user_data;
	REALLOC(ptr, size);
	return ptr;
}

static void
dr_free(void *ptr, void *user_data)
{
	(void)user_data;
	FREE(ptr);
}

Audio
audio_load(char *path)
{
	File file = map_file(path);
	if (!file.is_valid)
		return (Audio){ 0 };

	drmp3_allocation_callbacks callbacks = {
		.onMalloc = dr_malloc,
		.onRealloc = dr_realloc,
		.onFree = dr_free,
	};

	drmp3_config config;
	drmp3_uint64 frames;
	drmp3_int16 *data = drmp3_open_memory_and_read_pcm_frames_s16(
			file.data, file.size, &config, &frames, &callbacks);

	unmap_file(&file);

	ao_sample_format format = {
		.bits = 16,
		.channels = config.channels,
		.rate = config.sampleRate,
		.byte_format = AO_FMT_LITTLE,
	};

	int driver = ao_default_driver_id();
	ao_device *device = ao_open_live(driver, &format, NULL);
	if (!device) {
		VPERROR("Failed to open audio device: %s", strerror(errno));
		FREE(data);
		return (Audio){ 0 };
	}

	size_t size = frames * config.channels * sizeof(drmp3_int16);

	return (Audio){
		.data = (char *)data,
		.size = size,
		.device = device,
		.sample_rate = config.sampleRate,
		.channels = config.channels,
		.is_valid = true,
	};
}

void
audio_play(Audio *audio)
{
	ao_play(audio->device, audio->data, audio->size);
}

void
audio_play_offset(Audio *audio, size_t offset, size_t bytes)
{
	ao_play(audio->device, audio->data + offset, bytes);
}

void
audio_free(Audio *audio)
{
	FREE(audio->data);
	ao_close(audio->device);
	memset(audio, 0, sizeof(*audio));
}
