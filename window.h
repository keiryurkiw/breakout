/* Copyright (C) 2022, 2023  Keir Yurkiw */

#ifndef WINDOW_H
#define WINDOW_H

#include <stdbool.h>

typedef struct {
	unsigned int projection_ubo;

	void *background_ptr;
	void *music_ptr;
} WindowInfo;

bool glfw_init(void);
void *glfw_create_window(void);

/* This function should be ran with thrd_create(). */
int glfw_set_window_fps_title(void *window);

void glfw_cleanup(void *window);

#endif /* WINDOW_H */
