/* Copyright (C) Keir Yurkiw */

#version 330 core

out vec4 frag_colour;

in vec2 tex_coords;
in vec4 sprite_colour;

uniform sampler2D sprite_tex;

void main()
{
	frag_colour = sprite_colour * texture(sprite_tex, tex_coords);
}
