#version 330 core

out vec4 frag_colour;

in vec2 tex_coords;

uniform sampler2D glyph;
uniform vec4 text_colour;

void main()
{
	frag_colour = vec4(text_colour.xyz, texture(glyph, tex_coords).r);
}
