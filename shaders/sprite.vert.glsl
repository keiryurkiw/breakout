/* Copyright (C) Keir Yurkiw */

#version 330 core

layout (location = 0) in vec4 vertex; /* vec2 position, vec2 tex coords */
layout (location = 1) in mat4 model;
layout (location = 5) in vec4 colour;

out vec2 tex_coords;
out vec4 sprite_colour;

layout (std140) uniform matrices {
	mat4 projection;
};

void main()
{
	gl_Position = projection * model * vec4(vertex.xy, 0.0f, 1.0f);
	tex_coords = vertex.zw;
	sprite_colour = colour;
}
