#version 330 core

layout (location = 0) in vec4 vertex; /* vec2 pos, vec2 tex coords */

out vec2 tex_coords;

layout (std140) uniform matrices {
	mat4 projection;
};

void main()
{
	gl_Position = projection * vec4(vertex.xy, 0.0f, 1.0f);
	tex_coords = vertex.zw;
}
