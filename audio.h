/* Copyright (C) 2023  Keir Yurkiw */

#ifndef AUDIO_H
#define AUDIO_H

#include <ao/ao.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct {
	char *data;
	size_t size;

	ao_device *device;

	unsigned int sample_rate, channels;

	bool is_valid;
} Audio;

void audio_init(void);
void audio_deinit(void);

Audio audio_load(char *path);
void audio_play(Audio *audio);
void audio_play_offset(Audio *audio, size_t offset, size_t bytes);
void audio_free(Audio *audio);

#endif /* AUDIO_H */
