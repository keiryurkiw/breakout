/*
 * Copyright (C) 2022, 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <cglm/types.h>
#include <errno.h>
#include <math.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

void
hex_to_rgb(unsigned long hex, vec3 rgb)
{
	rgb[0] = (hex >> 16) / 255.0f;
	rgb[1] = ((hex & 0x00ff00) >> 8) / 255.0f;
	rgb[2] = (hex & 0x0000ff) / 255.0f;
}

float
clampf(float f, float lo, float hi)
{
	float t = (f < lo) ? lo : f;
	return (t > hi) ? hi : t;
}

int
clampi(int i, int lo, int hi)
{
	int t = (i < lo) ? lo : i;
	return (t > hi) ? hi : t;
}

char *
get_basename(char *path)
{
	char *name = strrchr(path, '/');
	return name ? name+1 : path;
}

char *
get_dirname(char *path)
{
	char *slash = strrchr(path, '/');
	if (!slash)
		return NULL;

	/* Length includes null terminator */
	ptrdiff_t length = slash - path;
	char *name = ALLOC(length);

	memcpy(name, path, length);
	name[length] = '\0';

	return name;
}

void *
_alloc(size_t size)
{
	void *ptr = malloc(size);
	if (!ptr) {
		VPERROR("Failed to allocate %zu bytes: %s", size, strerror(errno));

		/* TODO: exit safely */
		exit(errno);
	}

	return ptr;
}

void *
_realloc(void *ptr, size_t size)
{
	void *new_ptr = realloc(ptr, size);
	if (!new_ptr) {
		VPERROR("Failed to reallocate %zu bytes: %s", size, strerror(errno));
		FREE(ptr);

		/* TODO: exit safely */
		exit(errno);
	}

	return new_ptr;
}

#ifndef NDEBUG
void *
_debug_alloc(size_t size, char *file, int line)
{
	if (size == 0) {
		VPERROR("[%s:%d] Failed to allocate 0 bytes: Allocating 0 bytes is "
				"implementation defined", file, line);
		abort();
	}

	return _alloc(size);
}

void *
_debug_realloc(void *ptr, size_t size, char *file, int line)
{
	if (size == 0) {
		VPERROR("[%s:%d] Failed to allocate 0 bytes: Allocating 0 bytes is"
				"implementation defined", file, line);
		abort();
	}

	return _realloc(ptr, size);
}
#endif /* NDEBUG */

#ifdef _WIN32

#include "windowz-include.h"

static void
print_win_err(char *fmt, ...)
{
	enum { BUF_SIZE = 1024 };

	char buffer[BUF_SIZE];

	va_list args;
	va_start(args, fmt);
	vsprintf(buffer, fmt, args);
	va_end(args);

	char *win_buf = NULL;
	DWORD ret = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&win_buf, BUF_SIZE, NULL);
	if (!ret) {
		PERROR(buffer);
	} else {
		VPERROR("%s: %s", buffer, win_buf);
		LocalFree(win_buf);
	}
}

File
map_file(char *path)
{
	HANDLE handle = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (handle == INVALID_HANDLE_VALUE) {
		print_win_err("Failed to open file '%s'", path);
		return (File){ 0 };
	}

	SIZE_T size = 0;
	BOOL ret = GetFileSizeEx(handle, (PLARGE_INTEGER)&size);
	if (!ret) {
		print_win_err("Failed to get size of file '%s'", path);
		CloseHandle(handle);
		return (File){ 0 };
	} else if (size == 0) {
		VPERROR("Cannot map empty file '%s'", path);
		CloseHandle(handle);
		return (File){ 0 };
    }

	HANDLE map = CreateFileMappingA(handle, NULL, PAGE_READONLY, 0, 0, NULL);
	if (map == INVALID_HANDLE_VALUE) {
		print_win_err("Failed to map file '%s'", path);
		CloseHandle(handle);
		return (File){ 0 };
	}

	LPVOID data = MapViewOfFile(map, FILE_MAP_READ, 0, 0, size);
	if (!data) {
		print_win_err("Failed to map view of file '%s'", path);
		CloseHandle(map);
		CloseHandle(handle);
		return (File){ 0 };
	}

	CloseHandle(map);
	CloseHandle(handle);

	return (File){ .data = data, .size = size, .is_valid = true };
}

bool
unmap_file(File *file)
{
	if (!UnmapViewOfFile(file->data)) {
		print_win_err("Failed to unmap file");
		return false;
	}

	memset(file, 0, sizeof(*file));
	return true;
}

#else

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

File
map_file(char *path)
{
	int fd = open(path, O_RDONLY);
	if (fd < 0) {
		VPERROR("Failed to open file '%s': %s", path, strerror(errno));
		return (File){ 0 };
	}

	struct stat sb;
	if (fstat(fd, &sb) < 0) {
		VPERROR("Failed to get status from file '%s': %s",
				path, strerror(errno));
		close(fd);
		return (File){ 0 };
	} else if (sb.st_size == 0) {
		VPERROR("Failed to map empty '%s'", path);
		close(fd);
		return (File){ 0 };
	}

	void *data = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (data == MAP_FAILED) {
		VPERROR("Failed to map file '%s': %s", path, strerror(errno));
		close(fd);
		return (File){ 0 };
	}

	close(fd);

	return (File){ .data = data, .size = sb.st_size, .is_valid = true };
}

bool
unmap_file(File *file)
{
	if (munmap(file->data, file->size) < 0) {
		VPERROR("Failed to unmap file: %s", strerror(errno));
		return false;
	}

	memset(file, 0, sizeof(*file));
	return true;
}

#endif /* _WIN32 */

void
flip_image_on_load(bool b)
{
	stbi_set_flip_vertically_on_load(b);
}

Image
load_image(char *path)
{
	Image image;
	image.data = stbi_load(path, &image.width, &image.height,
			&image.num_channels, 0);
	if (!image.data) {
		VPERROR("Failed to load image '%s'", path);
		return (Image){ 0 };
	}

	return image;
}

void
free_image(Image *image)
{
	stbi_image_free(image->data);
	memset(image, 0, sizeof(*image));
}
