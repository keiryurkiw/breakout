/* Copyright (C) 2022, 2023  Keir Yurkiw */

#ifndef UTILS_H
#define UTILS_H

#include <cglm/types.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
	char *data;
	size_t size;

	bool is_valid;
} File;

typedef struct {
	unsigned char *data;
	int width, height;
	int num_channels;
} Image;

#define VPERROR(fmt, ...) fprintf(stderr, "breakout: " \
		"\x1b[01;31m" "error:" "\x1b[m" " " fmt "\n", __VA_ARGS__)
#define PERROR(msg) VPERROR("%s", msg)

#ifdef NDEBUG
#define ASSERT(expr, msg) ((void)0)
#else
#define ASSERT(expr, msg) if (!(expr)) { \
	VPERROR("[%s:%d] from %s(): Assertion '%s' FAILED: %s", \
			__FILE__, __LINE__, __func__, #expr, msg); \
	abort(); }
#endif /* NDEBUG */

void hex_to_rgb(unsigned long hex, vec3 rgb);

float clampf(float f, float lo, float hi);
int clampi(int i, int lo, int hi);

char *get_basename(char *path);
char *get_dirname(char *path);

void *_alloc(size_t size);
void *_realloc(void *ptr, size_t size);

#ifdef NDEBUG
#define ALLOC(size) _alloc(size)
#define REALLOC(ptr, size) (ptr = _realloc(ptr, size))
#define FREE(ptr) do{ free(ptr); ptr = NULL; }while(0)
#else

void *_debug_alloc(size_t size, char *file, int line);
void *_debug_realloc(void *ptr, size_t size, char *file, int line);

#define ALLOC(size) _debug_alloc(size, __FILE__, __LINE__)
#define REALLOC(ptr, size) (ptr = _debug_realloc(ptr, size, __FILE__, __LINE__))
#define FREE(ptr) do{ ASSERT(ptr != NULL, "Tried to free NULL pointer!"); \
	free(ptr); ptr = NULL; }while(0)
#endif /* NDEBUG */

File map_file(char *path);
bool unmap_file(File *file);

void flip_image_on_load(bool b);
Image load_image(char *path);
void free_image(Image *image);

#endif /* UTILS_H */
