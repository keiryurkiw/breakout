SRC = utils.c \
      audio.c \
      window.c \
      gameobj.c \
      level.c \
      text.c \
      sprite.c \
      opengl.c \
      main.c

HEADERS = utils.h \
          audio.h \
          sprite.h \
          text.h \
          level.h \
          gameobj.h \
          opengl.h \
          window.h

OBJ = $(SRC:.c=.o)
