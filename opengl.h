/* Copyright (C) 2022, 2023  Keir Yurkiw */

#ifndef OPENGL_H
#define OPENGL_H

#include <cglm/types.h>
#include <stdbool.h>
#include <stddef.h>

#include "sprite.h"

typedef struct {
	Sprite sprite;

	unsigned int program, vbo;
} Background;

bool ogl_init(void *win);

void ogl_set_vertex_attrib_pointer(size_t pos, size_t count,
		size_t stride, size_t offset);

unsigned int ogl_compile_shader(int type, char *path);
unsigned int ogl_link_shaders(unsigned int count, ...);
void ogl_delete_shaders(unsigned int count, ...);
unsigned int ogl_create_simple_shader(char **paths);

int ogl_get_unif_loc(unsigned int program, char *name);
void ogl_set_vec4(int loc, vec4 vector);
void ogl_set_mat4(int loc, mat4 matrix);

unsigned int ogl_allocate_ubo(size_t size, int usage, unsigned int bind_point,
		char *name, unsigned int nprograms, ...);
void ogl_set_ubo_mat4(unsigned int ubo, size_t offset, mat4 mat);

bool ogl_set_projection(void *win, int width, int height);

unsigned int ogl_gen_texture(char *path);

Background ogl_load_background(char *path, unsigned int program);
void ogl_set_background_size(Background *bg, float width, float height);
void ogl_draw_background(Background *bg);
void ogl_delete_background(Background *bg);

#endif /* OPENGL_H */
