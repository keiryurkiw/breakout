/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cglm/cglm.h>
#include <GL/glew.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "gameobj.h"
#include "level.h"
#include "opengl.h"
#include "sprite.h"
#include "utils.h"

extern vec2 initial_ball_velocity;

Level
level_load(char *path, Sprite block, Sprite solid_block,
		int win_width, int win_height)
{
	File file = map_file(path);
	if (!file.data)
		return (Level){ .is_valid = false };

	Level level = {
		.block = block,
		.solid_block = solid_block,
	};

	for (size_t i = 0; file.data[i] != '\n' && file.data[i] != '\0'; i++)
		if (file.data[i] != ' ')
			level.width += 1;

	for (size_t i = 0; file.data[i] != '\0'; i++)
		if (file.data[i] == '\n')
			level.height += 1;

	level.unit_width = win_width / (float)level.width;
	level.unit_height = win_height/1.5f / (float)level.height;

	for (size_t i = 0; i <= file.size/2; i++) {
		int tile = file.data[i*2] - '0';
		if (tile == 1)
			level.solid_block_count += 1;
		else if (tile > 1)
			level.block_count += 1;
	}

	level.solid_block_vbo = sprite_gen_info_vbo(&level.solid_block,
			level.solid_block_count, GL_STREAM_DRAW);
	level.block_vbo = sprite_gen_info_vbo(&level.block,
			level.block_count, GL_STREAM_DRAW);

	level.solid_block_bricks = ALLOC(level.solid_block_count *
			sizeof(*level.solid_block_bricks));
	level.block_bricks = ALLOC(level.block_count * sizeof(*level.block_bricks));

	size_t b_idx = 0, sb_idx = 0;
	for (size_t i = 0; i <= file.size/2; i++) {
		unsigned int x = i % level.width;
		unsigned int y = i / level.width;

		int tile = file.data[i*2] - '0';

		vec3 colour;
		switch (tile) {
			case 1:
				glm_vec3_copy((vec3){ 0.8f, 0.8f, 0.7f }, colour);
				break;
			case 2:
				glm_vec3_copy((vec3){ 0.2f, 0.6f, 1.0f }, colour);
				break;
			case 3:
				glm_vec3_copy((vec3){ 0.0f, 0.7f, 0.0f }, colour);
				break;
			case 4:
				glm_vec3_copy((vec3){ 0.8f, 0.8f, 0.4f }, colour);
				break;
			case 5:
				glm_vec3_copy((vec3){ 1.0f, 0.5f, 0.0f }, colour);
				break;
		}

		vec2 pos = { level.unit_width*x, level.unit_height*y };
		vec2 dimen = { level.unit_width, level.unit_height };
		if (tile == 1) {
			glm_vec2_copy(pos, level.solid_block_bricks[sb_idx].pos);
			glm_vec2_copy(dimen, level.solid_block_bricks[sb_idx].size);
			level.solid_block_bricks[sb_idx].is_destroyed = false;
			sprite_set_info(sb_idx++, &level.solid_block, level.solid_block_vbo,
					pos, dimen, colour);
		} else if (tile > 1) {
			glm_vec2_copy(pos, level.block_bricks[b_idx].pos);
			glm_vec2_copy(dimen, level.block_bricks[b_idx].size);
			level.block_bricks[b_idx].is_destroyed = false;
			sprite_set_info(b_idx++, &level.block, level.block_vbo,
					pos, dimen, colour);
		}
	}

	unmap_file(&file);

	size_t path_len = strlen(path);
	level.path = ALLOC(path_len + 1);
	strcpy(level.path, path);

	return level;
}

static Dir
get_collision_dir(vec2 target)
{
	vec2 compass[] = {
		{  0.0f,  1.0f }, /* North */
		{  1.0f,  0.0f }, /* East */
		{  0.0f, -1.0f }, /* South */
		{ -1.0f,  0.0f }, /* West */
	};

	float max = 0.0f;
	int best_match = -1;

	for (size_t i = 0; i < 4; i++) {
		vec2 target_norm;
		glm_vec2_normalize_to(target, target_norm);
		float dot = glm_vec2_dot(target_norm, compass[i]);
		if (dot > max) {
			max = dot;
			best_match = i;
		}
	}

	return best_match;
}

static Collision
check_collision(LevelBrick *brick, Gameobj *player, Ball *ball)
{
	vec2 radius, center;
	glm_vec2_div(ball->obj.size, (vec2){ 2.0f, 2.0f }, radius);
	glm_vec2_add(ball->obj.pos, radius, center);

	vec2 aabb_half = { 0 }, aabb_center = { 0 };
	if (brick) {
		glm_vec2_div(brick->size, (vec2){ 2.0f, 2.0f }, aabb_half);
		vec2 temp = {
			brick->pos[0] + aabb_half[0],
			brick->pos[1] + aabb_half[1],
		};
		glm_vec2_copy(temp, aabb_center);
	} else if (player) {
		glm_vec2_div(player->size, (vec2){ 2.0f, 2.0f }, aabb_half);
		vec2 temp = {
			player->pos[0] + aabb_half[0],
			player->pos[1] + aabb_half[1],
		};
		glm_vec2_copy(temp, aabb_center);
	}

	vec2 diff, diff_clamped;
	glm_vec2_sub(center, aabb_center, diff);
	diff_clamped[0] = clampf(diff[0], -aabb_half[0], aabb_half[0]);
	diff_clamped[1] = clampf(diff[1], -aabb_half[1], aabb_half[1]);

	vec2 closest;
	glm_vec2_add(aabb_center, diff_clamped, closest);

	glm_vec2_sub(closest, center, diff);

	float distance = glm_vec2_norm(diff);

	Collision col;
	if (distance < radius[0] && distance < radius[1]) {
		col.collided = true;
		col.dir = get_collision_dir(diff);
		glm_vec2_copy(diff, col.dir_vec);
	} else {
		col.collided = false;
		col.dir = DIR_NONE;
		glm_vec2_copy((vec2){ 0 }, col.dir_vec);
	}
	return col;
}

static void
move_ball_from_collision(Ball *ball, Collision *col)
{
	if (col->dir == DIR_EAST || col->dir == DIR_WEST) {
		ball->obj.velocity[0] = -ball->obj.velocity[0];

		float overlap = ball->obj.size[0] - fabsf(col->dir_vec[0]);
		ball->obj.pos[0] += (col->dir == DIR_WEST) ? overlap : -overlap;
	} else {
		ball->obj.velocity[1] = -ball->obj.velocity[1];

		float overlap = ball->obj.size[1] - fabsf(col->dir_vec[1]);
		ball->obj.pos[1] += (col->dir == DIR_NORTH) ? -overlap : overlap;
	}
}

bool
level_check_win(Level *level)
{
	for (size_t i = 0; i < level->block_count; i++)
		if (!level->block_bricks[i].is_destroyed)
			return false;

	return true;
}

void
level_check_collisions(Level *level, Gameobj *player, Ball *ball)
{
	for (size_t i = 0; i < level->block_count; i++) {
		if (level->block_bricks[i].is_destroyed)
			continue;

		Collision col = check_collision(&level->block_bricks[i], NULL, ball);
		if (col.collided) {
			sprite_set_info(i, &level->block, level->block_vbo,
					(vec2){ 0 }, (vec2){ 0 }, (vec3){ 0 });
			memset(&level->block_bricks[i], 0, sizeof(*level->block_bricks));
			level->block_bricks[i].is_destroyed = true;

			move_ball_from_collision(ball, &col);
		}
	}

	for (size_t i = 0; i < level->solid_block_count; i++) {
		Collision col = check_collision(&level->solid_block_bricks[i],
				NULL, ball);
		if (col.collided) {
			move_ball_from_collision(ball, &col);
		}
	}

	if (!ball->stuck) {
		Collision col = check_collision(NULL, player, ball);
		if (col.collided) {
			float center = player->pos[0] + player->size[0]/2.0f;
			float dist = ball->obj.pos[0] + ball->obj.size[0]/2.0f - center;
			float perc = dist / (player->size[0]/2.0f);

			float strength = 2.0f;
			vec2 old_vel;
			glm_vec2_copy(ball->obj.velocity, old_vel);
			ball->obj.velocity[0] = initial_ball_velocity[0] * perc * strength;
			ball->obj.velocity[1] = -fabsf(ball->obj.velocity[1]);

			float length = glm_vec2_norm(old_vel);
			glm_vec2_normalize(ball->obj.velocity);
			glm_vec2_scale(ball->obj.velocity, length, ball->obj.velocity);
		}
	}
}

void
level_draw(Level *level, unsigned int program)
{
	glUseProgram(program);
	glActiveTexture(GL_TEXTURE0);

	glBindVertexArray(level->block.vao);
	glBindTexture(GL_TEXTURE_2D, level->block.texture);
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, level->block_count);

	glBindVertexArray(level->solid_block.vao);
	glBindTexture(GL_TEXTURE_2D, level->solid_block.texture);
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, level->solid_block_count);
}

void
level_free(Level *level)
{
	FREE(level->path);
	FREE(level->block_bricks);
	FREE(level->solid_block_bricks);
	glDeleteBuffers(1, &level->block_vbo);
	glDeleteBuffers(1, &level->solid_block_vbo);
	memset(level, 0, sizeof(*level));
}

void
level_reload(Level *level, int win_width, int win_height)
{
	Level new_level = level_load(level->path, level->block,
			level->solid_block, win_width, win_height);
	level_free(level);
	*level = new_level;
}
