/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cglm/cglm.h>
#include <GL/glew.h>
#include <stddef.h>
#include <string.h>

#include "opengl.h"
#include "sprite.h"

Sprite
sprite_create(char *tex_path)
{
	const float vertices[] = {
		/* pos       tex */
		1.0f, 0.0f,  1.0f, 0.0f,
		1.0f, 1.0f,  1.0f, 1.0f,
		0.0f, 0.0f,  0.0f, 0.0f,
		0.0f, 1.0f,  0.0f, 1.0f,
	};

	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	ogl_set_vertex_attrib_pointer(0, 4, 4 * sizeof(float), 0);

	/* model matrix */
	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);

	/* colour vector */
	glVertexAttribDivisor(5, 1);

	unsigned int texture = ogl_gen_texture(tex_path);

	return (Sprite){ .vao = vao, .vbo = vbo, .texture = texture };
}

unsigned int
sprite_gen_info_vbo(Sprite *sprite, unsigned int count, int usage)
{
	glBindVertexArray(sprite->vao);

	unsigned int vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	const size_t size = count * (sizeof(mat4) + sizeof(vec4));
	glBufferData(GL_ARRAY_BUFFER, size, NULL, usage);

	const size_t stride = sizeof(mat4) + sizeof(vec4);

	/* model matrix */
	ogl_set_vertex_attrib_pointer(1, 4, stride, 0 * sizeof(vec4));
	ogl_set_vertex_attrib_pointer(2, 4, stride, 1 * sizeof(vec4));
	ogl_set_vertex_attrib_pointer(3, 4, stride, 2 * sizeof(vec4));
	ogl_set_vertex_attrib_pointer(4, 4, stride, 3 * sizeof(vec4));

	/* colour vector */
	ogl_set_vertex_attrib_pointer(5, 4, stride, 4 * sizeof(vec4));

	return vbo;
}

void
sprite_set_info(size_t index, Sprite *sprite, unsigned int vbo,
		vec2 pos, vec2 dimen, vec3 colour)
{
	glBindVertexArray(sprite->vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	mat4 model = GLM_MAT4_IDENTITY_INIT;
	glm_translate(model, (vec3){ pos[0], pos[1], 0.0f });
	glm_scale(model, (vec3){ dimen[0], dimen[1], 1.0f });

	vec4 v4_colour;
	glm_vec4(colour, 1.0f, v4_colour);

	size_t offset = index * (sizeof(mat4) + sizeof(vec4));
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(mat4), model);
	offset += sizeof(mat4);
	glBufferSubData(GL_ARRAY_BUFFER, offset, sizeof(vec4), v4_colour);
}

void
sprite_free(Sprite *sprite)
{
	glDeleteTextures(1, &sprite->texture);
	glDeleteBuffers(1, &sprite->vbo);
	glDeleteVertexArrays(1, &sprite->vbo);

	memset(sprite, 0, sizeof(*sprite));
}
