/* Copyright (C) 2023  Keir Yurkiw */

#ifndef TEXT_H
#define TEXT_H

#include <cglm/types.h>
#include <stdbool.h>

#include "sprite.h"

typedef struct {
	ivec2 size, bearing;
	unsigned int advance;

	unsigned int texture;
} Char;

typedef struct {
	Char chars[128];
	unsigned int vao, vbo;

	bool is_valid;
} Font;

void *text_init(void);
Font text_load_font(void *ft, char *path, unsigned int size);
void text_render(char *str, Font *font, vec2 pos, float scale,
		unsigned long col, unsigned int prog);
float text_get_len(char *str, Font *font, float scale);
void text_delete_font(Font *font);
void text_deinit(void **ft);

#endif /* TEXT_H */
