/* Copyright (C) 2023  Keir Yurkiw */

#ifndef SPRITE_H
#define SPRITE_H

#include <cglm/types.h>
#include <stddef.h>

typedef struct {
	unsigned int vao, vbo;
	unsigned int texture;
} Sprite;

Sprite sprite_create(char *tex_path);
unsigned int sprite_gen_info_vbo(Sprite *sprite, unsigned int count, int usage);
void sprite_set_info(size_t index, Sprite *sprite, unsigned int vbo,
		vec2 pos, vec2 dimen, vec3 colour);
void sprite_free(Sprite *sprite);

#endif /* SPRITE_H */
