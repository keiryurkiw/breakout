/* Copyright (C) 2023  Keir Yurkiw */

#ifndef GAMEOBJ_H
#define GAMEOBJ_H

#include <cglm/types.h>
#include <stdbool.h>

#include "sprite.h"

typedef struct {
	Sprite sprite;

	vec3 colour;
	vec2 pos, size;
	float rotation;

	unsigned int vbo;

	vec2 velocity;
} Gameobj;

typedef struct {
	Gameobj obj;

	bool stuck;
} Ball;

Gameobj gameobj_create(char *tex_path);
void gameobj_set_info(Gameobj *obj);
void gameobj_draw(Gameobj *obj, unsigned int program);
void gameobj_free(Gameobj *obj);

Ball ball_create(char *tex_path, float radius);
void ball_move(Ball *ball, int win_width);
void ball_free(Ball *ball);

#endif /* GAMEOBJ_H */
